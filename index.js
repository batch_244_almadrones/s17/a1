/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/

//first function here:

function ask_question(question) {
  let data = prompt(question);
  return data;
}

let f_name = ask_question("What is your name?");
let age = ask_question("How old are you?");
let loc = ask_question("Where do you reside?");

console.log("Hello, " + f_name + "!");
console.log("You are " + age + " years old.");
console.log("You live in " + loc + ".");

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

//second function here:
let best_music_artists = [
  "December Avenue",
  "Ben&Ben",
  "Silent Sanctuary",
  "Taylor Swift",
  "Ed Sheeran",
];

function five_best_music_artists() {
  console.log("1. " + best_music_artists[0]);
  console.log("2. " + best_music_artists[1]);
  console.log("3. " + best_music_artists[2]);
  console.log("4. " + best_music_artists[3]);
  console.log("5. " + best_music_artists[4]);
}

five_best_music_artists();
/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

//third function here:
let movie_name, rotten_rating;
let best_movies = {
  movie_name: [
    "Harry Potter",
    "Hunger Games",
    "Avengers: Endgame",
    "Knives Out",
    "Star Wars",
  ],
  rotten_rating: [96, 90, 94, 92, 91],
};

function five_best_movies_w_rating() {
  console.log(
    "1. " +
      best_movies.movie_name[0] +
      "Rotten Tomatoes Rating: " +
      best_movies.rotten_rating[0] +
      "%"
  );
  console.log(
    "2. " +
      best_movies.movie_name[1] +
      "Rotten Tomatoes Rating: " +
      best_movies.rotten_rating[1] +
      "%"
  );
  console.log(
    "3. " +
      best_movies.movie_name[2] +
      "Rotten Tomatoes Rating: " +
      best_movies.rotten_rating[2] +
      "%"
  );
  console.log(
    "4. " +
      best_movies.movie_name[3] +
      "Rotten Tomatoes Rating: " +
      best_movies.rotten_rating[3] +
      "%"
  );
  console.log(
    "5. " +
      best_movies.movie_name[4] +
      "Rotten Tomatoes Rating: " +
      best_movies.rotten_rating[4] +
      "%"
  );
}

five_best_movies_w_rating();
/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

// printUsers();

let printFriends = function () {
  alert("Hi! Please add the names of your friends.");
  let friend1 = prompt("Enter your first friend's name:");
  let friend2 = prompt("Enter your second friend's name:");
  let friend3 = prompt("Enter your third friend's name:");

  console.log("You are friends with:");
  console.log(friend1);
  console.log(friend2);
  console.log(friend3);
};

// console.log(friend1);
// console.log(friend2);

printFriends();
